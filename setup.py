from setuptools import setup
import os

version = '0.1.2'

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()

setup(name='django-business-workflow',
      version=version,
      description="Generic workflow engine for Django.",
      long_description=README,
      classifiers=[
          "Programming Language :: Python",
          "Programming Language :: Python :: 2.6",
          "Programming Language :: Python :: 2.7",
          "Programming Language :: Python :: 3.3",
          "Operating System :: OS Independent",
          "Topic :: Software Development :: Libraries",
          "Topic :: Utilities",
          "Environment :: Web Environment",
          "Framework :: Django",
      ],
      keywords='django business workflow',
      author='Ayme Perdomo Alonso',
      author_email='aymeperdomo@gmail.com',
      license='BSD',
      packages=['business_workflow'],
      namespace_packages=[],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'django-permissions == 1.0.3',
          'django-workflows == 1.0.2',
      ],
)
