# coding=utf-8
from django.db import models

from business_workflow.decorators import workflow_enabled


@workflow_enabled
class Model1(models.Model):
    name = models.CharField(max_length=100)


@workflow_enabled
class Model2(models.Model):
    name = models.CharField(max_length=100)


@workflow_enabled
class ModelBase1(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True


class ModelChild1(ModelBase1):
    name_child = models.CharField(max_length=100)


class ModelChild21(ModelBase1):
    name_second_child = models.CharField(max_length=100)

