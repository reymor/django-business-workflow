# coding=utf-8

import os
from django.core import serializers
from django.conf import settings

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.models import FlatPage
from django.db.models.loading import get_apps, get_models
from django.test import TestCase
import permissions.utils
from test_workflow.models import Model1

from workflows import utils
from workflows.models import Workflow, State, WorkflowModelRelation, WorkflowPermissionRelation, \
    StatePermissionRelation, StateInheritanceBlock

from business_workflow.models import WorkflowTransition, WorkflowHistorical, BusinessState, \
    WorkflowTransitionActionClass


class ModelWorkflowBaseTestCase(TestCase):
    """Tests workflow base models.
    """

    def setUp(self):
        """
        """
        create_workflow(self)
        self.apps = get_apps()

    def test_save(self):
        """
        """
        ctype = ContentType.objects.get_for_model(Model1)
        self.model = Model1.objects.get(name=self.name)
        self.assertIsNotNone(self.workflow, msg='Not None')
        self.workflow_historical = WorkflowHistorical.objects.get(
            content_type=ctype)
        self.assertIsNotNone(self.workflow_historical, msg='Not None')

    def test_get_workflow(self):
        """
        """
        utils.set_workflow(self.model, self.workflow)
        result = utils.get_workflow(self.model)
        self.assertEqual(result, self.workflow)

        # now check we can find it in the database again
        all_model1_in_database = Model1.objects.all()
        self.assertEquals(len(all_model1_in_database), 1)
        only_model1_in_database = all_model1_in_database[0]
        self.assertEquals(only_model1_in_database, self.model)

        # and check that it's saved its one attribute: name
        self.assertEquals(only_model1_in_database.name, self.name)

    def test_get_current_state(self):
        """
        """
        result = utils.get_state(self.model)
        self.assertIsNotNone(result, None)
        utils.set_workflow(self.model, self.workflow)
        result = utils.get_state(self.model)
        self.assertEqual(result, self.workflow.initial_state)

        # get states
        states = self.workflow.states.all()
        self.assertEqual(states[0], self.private)
        self.assertEqual(states[1], self.public)

    def test_build_history_comment(self):
        """
        """
        comment = "test build history comment"
        self.assertTrue(comment)
        self.assertFalse(self.assertIsNotNone(comment, msg="Not None"))

    def test_get_allowed_transitions(self):
        """
        """
        self.view = permissions.utils.register_permission("Publish", "publish")
        transitions = self.private.get_allowed_transitions(self.page_1, self.user)
        self.assertEqual(len(transitions), 1)

        # protect the transition with a permission
        self.make_public.permission = self.view
        self.make_public.save()

        # user has no transition
        transitions = self.private.get_allowed_transitions(self.page_1, self.user)
        self.assertEqual(len(transitions), 0)

        # grant permission
        permissions.utils.grant_permission(self.page_1, self.owner, self.view)

        # user has transition again
        transitions = self.private.get_allowed_transitions(self.page_1, self.user)
        self.assertEqual(len(transitions), 1)

    def test_evaluation_transitions_conditions(self):
        """
        """
        #TODO: por hacer!
        pass

    def test_execute_transition(self):
        """
        """
        wtac = WorkflowTransitionActionClass.objects.create(name="Impresora", class_name="Impresora",
                                                            module_name="test_workflow")
        wt = WorkflowTransition.objects.create(name="Public", workflow=self.workflow, destination=self.public,
                                               description="test workflow transition", class_name=wtac,
                                               method_name="imprimir")
        # execute transition (if transition.class_name)
        self.assertTrue(self.model.execute_transition(self.user, "execute transition", wt))

        # execute transition (if not transition.class_name)
        #self.assertTrue(self.model.execute_transition(self.user, "execute transition", self.make_public))

    def test_set_state(self):
        """
        """
        # Permissions
        result = permissions.utils.has_permission(self.page_1, self.user, "edit")
        self.assertEqual(result, True)

        result = permissions.utils.has_permission(self.page_1, self.user, "view")
        self.assertEqual(result, True)

        # Inheritance
        result = permissions.utils.is_inherited(self.page_1, "view")
        self.assertEqual(result, False)

        result = permissions.utils.is_inherited(self.page_1, "edit")
        self.assertEqual(result, False)

        # Change state
        utils.set_state(self.page_1, self.public)

        # Permissions
        result = permissions.utils.has_permission(self.page_1, self.user, "edit")
        self.assertEqual(result, False)

        result = permissions.utils.has_permission(self.page_1, self.user, "view")
        self.assertEqual(result, True)

        # Inheritance
        result = permissions.utils.is_inherited(self.page_1, "view")
        self.assertEqual(result, True)

        result = permissions.utils.is_inherited(self.page_1, "edit")
        self.assertEqual(result, False)

    def test_update_permissions(self):
        """
        """
        self.assertIsNone(self.model.update_permissions())

    def is_in_business_state(self):
        """
        """
        states = self.workflow.states.all()
        self.assertEqual(states, self.bs.workflow_state)

    #def tearDown(self):
    #    objects = []
    #    for app in self.apps:
    #        for model in get_models(app):
    #            objects.extend(model._default_manager.all())
    #
    #    out = open(os.path.join(settings.SVN_DIR, 'fixtures', 'tests.json'), 'w')
    #    out.write(serializers.serialize('json', objects))
    #    out.close()


class BusinessStateTestCase(TestCase):
    def setUp(self):
        """
        """
        create_workflow(self)
        self.apps = get_apps()

    def test_unicode(self):
        """
        """
        self.assertEquals(self.start.__unicode__(), u"Start (WfModel1)")

    def tearDown(self):
        objects = []
        for app in self.apps:
            for model in get_models(app):
                objects.extend(model._default_manager.all())

        out = open(os.path.join(settings.SVN_DIR, 'fixtures', 'tests.json'), 'w')
        out.write(serializers.serialize('json', objects))
        out.close()


class WorkflowHistoricalManagerTestCase(TestCase):
    def setUp(self):
        """
        """
        create_workflow(self)
        self.apps = get_apps()

    def test_get_history_from_object_query_set(self):
        """
        """
        ctype = ContentType.objects.get_for_model(Model1)
        wh = WorkflowHistorical(content_type=ctype)
        self.assertTrue(wh)

    def test_get_elements_for_user(self):
        """
        """
        ctype = ContentType.objects.get_for_model(Model1)
        wh = WorkflowHistorical(content_type=ctype, user=self.user)
        self.assertTrue(wh)

    def tearDown(self):
        objects = []
        for app in self.apps:
            for model in get_models(app):
                objects.extend(model._default_manager.all())

        out = open(os.path.join(settings.SVN_DIR, 'fixtures', 'tests.json'), 'w')
        out.write(serializers.serialize('json', objects))
        out.close()

# Helpers ####################################################################

def create_workflow(self):
    self.workflow = Workflow.objects.create(name="WfModel1")
    self.private = State.objects.create(name="Private", workflow=self.workflow)
    self.public = State.objects.create(name="Public", workflow=self.workflow)
    self.start = State.objects.create(name="Start", workflow=self.workflow)

    self.make_public = WorkflowTransition.objects.create(name="Make public", workflow=self.workflow,
                                                         destination=self.public)
    self.make_private = WorkflowTransition.objects.create(name="Make private", workflow=self.workflow,
                                                          destination=self.private)

    self.private.transitions.add(self.make_public)
    self.public.transitions.add(self.make_private)

    self.workflow.initial_state = self.private

    self.wmr = WorkflowModelRelation.objects.create(content_type=ContentType.objects.get_for_model(Model1),
                                                    workflow=self.workflow)
    self.workflow.save()

    self.name = 'Model1'
    self.model = Model1.objects.create(name=self.name)


    # Register roles
    self.anonymous = permissions.utils.register_role("Anonymous")
    self.owner = permissions.utils.register_role("Owner")

    self.user = User.objects.create(username="user1")
    permissions.utils.add_role(self.user, self.owner)

    # Example content type
    self.page_1 = FlatPage.objects.create(url="/page-1/", title="Page 1")

    # Registers permissions
    self.view = permissions.utils.register_permission("View", "view")
    self.edit = permissions.utils.register_permission("Edit", "edit")

    # Add all permissions which are managed by the workflow
    self.wpr = WorkflowPermissionRelation.objects.create(workflow=self.workflow, permission=self.view)
    self.wpr = WorkflowPermissionRelation.objects.create(workflow=self.workflow, permission=self.edit)

    # Add permissions for single states
    self.spr = StatePermissionRelation.objects.create(state=self.public, permission=self.view, role=self.owner)
    self.spr = StatePermissionRelation.objects.create(state=self.private, permission=self.view, role=self.owner)
    self.spr = StatePermissionRelation.objects.create(state=self.private, permission=self.edit, role=self.owner)

    # Add inheritance block for single states
    self.sib = StateInheritanceBlock.objects.create(state=self.private, permission=self.view)
    self.sib = StateInheritanceBlock.objects.create(state=self.private, permission=self.edit)
    self.sib = StateInheritanceBlock.objects.create(state=self.public, permission=self.edit)

    utils.set_workflow(self.page_1, self.workflow)

    # Add business states
    self.bs = BusinessState.objects.create(name="Start", description="Iniciando el proceso")
    self.bs.workflow_state.add(self.start)
