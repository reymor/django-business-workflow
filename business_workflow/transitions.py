# coding=utf-8

from django.utils.translation import ugettext_lazy as _
from workflows.utils import permissions
from permissions.models import Role

from workflows.models import Transition
from workflows.models import StateObjectRelation
from workflows.models import WorkflowPermissionRelation
from workflows.models import StatePermissionRelation
from workflows.models import StateInheritanceBlock
from .models import WorkflowTransition
from .models import WorkflowHistorical


class ActionTransitionBase(object):
    """
        Class that implements the action.
    """
    instance = None
    transition = None
    comment = u""
    initial_state = None
    user = None
    result = False
    kwargs_set = False

    def get_params_from_kwargs(self, **kwargs):

        if not self.kwargs_set:
            if 'instance' in kwargs and kwargs['instance']:
                self.instance = kwargs['instance']
            else:
                raise ValueError(_(u"Required a param called instance"))

            if 'transition' in kwargs and kwargs['transition']:
                self.transition = kwargs['transition']
            else:
                raise ValueError(_(u"Required a param called transition"))

            if 'user' in kwargs and kwargs['user']:
                self.user = kwargs['user']
            else:
                raise ValueError(_(u"Required a param called user"))

            if 'comment' in kwargs:
                self.comment = kwargs['comment']

            self.kwargs_set = True

    def set_workflow_transition(self):
        if not isinstance(self.transition, WorkflowTransition):
            if not isinstance(self.transition, Transition):
                self.transition = WorkflowTransition.objects.get(name=self.transition)
            else:
                self.transition = WorkflowTransition.objects.get(name=self.transition.name)

    def update_permissions(self):
        """
            Updates the permissions of the instance according to the object's current workflow state.
        """
        workflow = self.instance.mw_workflow
        state = self.instance.mw_state

        # Remove all permissions for the workflow
        for role in Role.objects.all():
            for wpr in WorkflowPermissionRelation.objects.filter(workflow=workflow):
                permissions.utils.remove_permission(self.instance, role, wpr.permission)

        # Grant permission for the state
        for spr in StatePermissionRelation.objects.filter(state=state):
            permissions.utils.grant_permission(self.instance, spr.role, spr.permission)

        # Remove all inheritance blocks from the object
        for wpr in WorkflowPermissionRelation.objects.filter(workflow=workflow):
            permissions.utils.remove_inheritance_block(self.instance, wpr.permission)

        # Add inheritance blocks of this state to the object
        for sib in StateInheritanceBlock.objects.filter(state=state):
            permissions.utils.add_inheritance_block(self.instance, sib.permission)

    def set_state(self):
        """
            Sets the state for the passed object to the passed state and updates the permissions for the instance.
        """
        content_type = self.instance.mw_content_type
        state = self.transition.destination
        try:
            sor = StateObjectRelation.objects.get(content_type=content_type, content_id=self.instance.pk)
        except StateObjectRelation.DoesNotExist:
            sor = StateObjectRelation.objects.create(content=self.instance, state=state)
        else:
            sor.state = state
            sor.save()
        self.instance.mw_state = state
        self.instance.current_state = state
        self.instance.save()
        self.update_permissions()

    def __call__(self, **kwargs):
        """
            Processes the passed transition to the passed object (if allowed).
        """
        self.get_params_from_kwargs(**kwargs)
        try:
            self.set_workflow_transition()
        except WorkflowTransition.DoesNotExist:
            self.result = False
            return self.result

        if self.transition in self.instance.get_allowed_transitions(self.user):
            self.initial_state = self.instance.mw_state
            self.set_state()
            WorkflowHistorical.objects.create(content_type=self.instance.mw_content_type,
                                              content_id=self.instance.pk,
                                              user=self.user,
                                              initial_state=self.initial_state,
                                              transition=self.transition,
                                              comment=self.build_history_comment())
            self.result = True
            return self.result
        else:
            self.result = False
            return self.result

    def build_history_comment(self):
        return self.comment

    def generic_method(self, **kwargs):
        return self.__call__(**kwargs)



