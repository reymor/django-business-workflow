from django.core.exceptions import ObjectDoesNotExist


class WorkflowDoesNotExist(ObjectDoesNotExist):
    pass
