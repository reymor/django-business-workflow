# coding=utf-8

import ast
import logging

# django imports
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

# django imports
from django.core.exceptions import PermissionDenied
from django.db.models.base import Model
from django.utils.module_loading import import_by_path

# django_workflow imports
from workflows.models import Workflow
from workflows.models import WorkflowObjectRelation
from workflows.models import StateObjectRelation
from workflows.models import WorkflowPermissionRelation
from workflows.models import StatePermissionRelation
from workflows.models import StateInheritanceBlock
from workflows.models import State
from workflows.models import Transition
from workflows.utils import get_workflow_for_model, permissions

# permissions imports
import permissions
from permissions.models import Role

# new featuring imports
from .exceptions import WorkflowDoesNotExist

logger = logging.getLogger(__name__)


class BusinessState(models.Model):
    """
        Class for to group the workflow state in business states.
    """
    name = models.CharField(max_length=50, verbose_name=_(u'Name'))
    description = models.TextField(verbose_name=_(u'Description'), null=True, blank=True)
    workflow_state = models.ManyToManyField(State, verbose_name=_(u'Workflow states'), related_name="workflow_states")

    def __unicode__(self):
        return self.name


class WorkflowTransitionActionClass(models.Model):
    """
        Class to save the action implementation for the workflow transition.
    """
    name = models.CharField(_(u"Action name"), max_length=50)
    class_name = models.CharField(_(u"Class name"), max_length=50)
    module_name = models.CharField(_(u"Module name"), max_length=50)


class WorkflowTransition(Transition):
    """
        Class to save custom Transition.
    """
    description = models.TextField(_(u"Description"), null=True, blank=True)
    class_name = models.ForeignKey(WorkflowTransitionActionClass,
                                   verbose_name=(_(u"Class that implement the action")),
                                   null=True, blank=True)
    method_name = models.CharField(_(u"Method name that implement the action"), max_length=50, null=True,
                                   blank=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.workflow.name)


class WorkflowHistoricalManager(models.Manager):
    """
        WorkflowHistoricalManager class.
    """

    def get_history_from_object_query_set(self, object):
        content_type = ContentType.objects.get_for_model(object)
        return self.filter(content_type=content_type, content_id=object.pk)

    def get_elements_for_user(self, object, user):
        content_type = ContentType.objects.get_for_model(object)
        return self.filter(content_type=content_type, content_id=object.id, user=user)


class WorkflowHistorical(models.Model):
    """
        Model class to save the historic of workflow.
    """
    content_type = models.ForeignKey(ContentType, verbose_name=_(u"Content type"),
                                     related_name="content_type_set_for_%(class)s")
    content_id = models.PositiveIntegerField(_(u"Content id"))
    content_object = generic.GenericForeignKey('content_type', 'content_id')
    user = models.ForeignKey(User, verbose_name=_(u"User"), null=True, blank=True)
    initial_state = models.ForeignKey(State, verbose_name=_(u"Initial state"))
    transition = models.ForeignKey(WorkflowTransition, verbose_name=_(u"Transition"), blank=True, null=True)
    update_at = models.DateTimeField(_(u"Update at"), auto_now_add=True)
    comment = models.TextField(_(u"User comment"), null=True, blank=True)
    objects = WorkflowHistoricalManager()


class ModelWorkflowBase(object):
    """
        ModelWorkflowBase class.
    """

    def __init__(self, *args, **kwargs):
        Model.__init__(self, *args, **kwargs)
        self.mw_content_type = ContentType.objects.get_for_model(self)
        self.mw_workflow = get_workflow_for_model(self.mw_content_type)
        if not self.mw_workflow:
            try:
                self.mw_workflow = Workflow.objects.get(pk=self.get_workflow_id())
            except Workflow.DoesNotExist:
                msg = _(u"Not exists the workflow: ") + self.mw_content_type.__str__()
                logger.error(msg)

                raise WorkflowDoesNotExist(msg)
            else:
                self.mw_workflow.set_to_model(ctype=self.mw_content_type)

        if self.pk:
            workflow = self.get_workflow()
            if not workflow:
                self.mw_workflow.set_to_object(self)
                self.mw_state = self.mw_workflow.get_initial_state()
                WorkflowHistorical.objects.create(content_type=self.mw_content_type, content_id=self.pk,
                                                  initial_state=self.mw_state,
                                                  comment=self.build_history_comment())
            else:
                self.mw_state = self.get_current_state()
                #self.mw_workflow = self.mw_state.workflow

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, comment=u"", user=None):
        if not self.pk:
            new = True
        else:
            new = False

        Model.save(self, force_insert, force_update, using, update_fields)
        if new and self.pk:
            self.mw_workflow.set_to_object(self)
            self.mw_state = self.mw_workflow.get_initial_state()
            self.set_state()
            WorkflowHistorical.objects.create(content_type=self.mw_content_type, content_id=self.pk,
                                              initial_state=self.mw_state, user=user,
                                              comment=self.build_history_comment(comment))

    def get_workflow(self):
        """
            Returns the object workflow from database.
        """
        try:
            wor = WorkflowObjectRelation.objects.get(content_id=self.pk, content_type=self.mw_content_type)
        except WorkflowObjectRelation.DoesNotExist:
            return None
        else:
            return wor.workflow

    def get_current_state(self):
        """
            Returns the current workflow state from database.
        """
        try:
            sor = StateObjectRelation.objects.get(content_type=self.mw_content_type, content_id=self.id)
        except StateObjectRelation.DoesNotExist:
            return None
        else:
            return sor.state

    def build_history_comment(self, comment=u""):
        return comment

    def get_allowed_transitions(self, user):
        """
            Return the transitions allow from user.
        """
        transitions = []
        for transition in self.mw_state.transitions.all():
            permission = transition.permission
            if permission is None:
                transitions.append(transition.id)
            else:
                # First we try to get the objects specific has_permission
                # method (in case the object inherits from the PermissionBase
                # class).
                try:
                    if self.has_permission(user, permission.codename):
                        transitions.append(transition.id)
                except AttributeError:
                    if permissions.utils.has_permission(self, user, permission.codename):
                        transitions.append(transition.id)
        transitions_query = WorkflowTransition.objects.filter(pk__in=transitions)

        return transitions_query

    def evaluate_conditions(self, transitions):

        if len(transitions) > 1:
            for sec in transitions:
                try:
                    passed = False
                    conditions = ast.literal_eval(sec.condition)
                    for condition in conditions:
                        method = getattr(self, condition['method'])
                        operator = condition['operator']
                        result = ast.literal_eval(condition['result'])
                        if operator.upper() == 'IS' and method(sec) == result:
                            return [sec]
                        if (operator.upper() == 'NOT' and
                                not method() == result):
                            return [sec]
                        if operator.upper() == '>' and method(sec) > result:
                            return [sec]
                        if operator.upper() == '<' and method(sec) < result:
                            return [sec]

                except Exception, e:
                    raise e
                    return []
        else:
            return transitions

    def execute_transition(self, user, comment, transition, **kwargs):

        if user is None:
            raise PermissionDenied(_(u'User is required..'))

        transition_action_method = transition.method_name
        if not transition_action_method:
            transition_action_method = 'generic_method'

        if not transition.class_name:
            building_dynamic_object = 'business_workflow.transitions.ActionTransitionBase'
            transition_instance = import_by_path(building_dynamic_object)()
        else:
            building_dynamic_object = transition.class_name.module_name + '.transitions.' + transition.class_name. \
                class_name
            transition_instance = import_by_path(building_dynamic_object)()

        comment = self.build_history_comment(comment)
        transition_instance_method = getattr(transition_instance, transition_action_method)
        params = dict({"instance": self,
                       "user": user,
                       "transition": transition,
                       "comment": comment},
                      **kwargs)
        return transition_instance_method(**params)

    def set_state(self):
        """
            Sets the state for the passed object to the passed state and updates the permissions for the instance.
        """
        content_type = self.mw_content_type
        state = self.mw_state
        try:
            sor = StateObjectRelation.objects.get(content_type=content_type, content_id=self.pk)
        except StateObjectRelation.DoesNotExist:
            sor = StateObjectRelation.objects.create(content=self, state=state)
        else:
            sor.state = state
            sor.save()
        self.update_permissions()

    def update_permissions(self):
        """
            Updates the permissions of the instance according to the object's current workflow state.
        """
        workflow = self.mw_workflow
        state = self.mw_state

        # Remove all permissions for the workflow
        for role in Role.objects.all():
            for wpr in WorkflowPermissionRelation.objects.filter(workflow=workflow):
                permissions.utils.remove_permission(self, role, wpr.permission)

        # Grant permission for the state
        for spr in StatePermissionRelation.objects.filter(state=state):
            permissions.utils.grant_permission(self, spr.role, spr.permission)

        # Remove all inheritance blocks from the object
        for wpr in WorkflowPermissionRelation.objects.filter(workflow=workflow):
            permissions.utils.remove_inheritance_block(self, wpr.permission)

        # Add inheritance blocks of this state to the object
        for sib in StateInheritanceBlock.objects.filter(state=state):
            permissions.utils.add_inheritance_block(self, sib.permission)

    def is_in_business_state(self, business_state_id):
        return True if State.objects.filter(workflow_states__id=business_state_id, id=self.mw_state.id) else False