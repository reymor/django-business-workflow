from django.contrib import admin

# Register your models here.
from django.contrib.admin.widgets import FilteredSelectMultiple
from django import forms
from django.forms.models import ModelMultipleChoiceField

# django_workflow imports
from workflows.models import State, Transition

# new features imports
from .models import BusinessState
from .models import WorkflowHistorical
from .models import WorkflowTransitionActionClass, WorkflowTransition


class TransitionMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        main_str = ModelMultipleChoiceField.label_from_instance(self, obj)
        return '%s(%s)' % (main_str, obj.workflow)


class StateForm(forms.ModelForm):
    transitions = TransitionMultipleChoiceField(label='Transitions',
                                                widget=FilteredSelectMultiple(verbose_name="Selection",
                                                                              is_stacked=False),
                                                queryset=Transition.objects.all())

    class Meta:
        model = State
        fields = ('name', 'workflow', 'transitions')


class StateOptions(admin.ModelAdmin):
    list_display = ('name', 'workflow')
    list_filter = ('workflow',)
    form = StateForm


class WorkflowTransitionAdmin(admin.ModelAdmin):
    list_display = ('name', 'workflow', 'destination')
    list_filter = ('workflow',)


class BusinessStateForm(forms.ModelForm):
    business_wf_state = ModelMultipleChoiceField(label='Workflows',
                                               widget=FilteredSelectMultiple(verbose_name="Selection",
                                                                             is_stacked=False),
                                               queryset=State.objects.all())

    class Meta:
        model = BusinessState
        fields = ('name', 'description', 'workflow_state')


class BusinessStateOptions(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')
    form = BusinessStateForm


class WorkflowHistoricalOptions(admin.ModelAdmin):
    list_display = ('content_type', 'content_id')


admin.site.register(BusinessState, BusinessStateOptions)
admin.site.unregister(State)
admin.site.register(State, StateOptions)
admin.site.register(WorkflowHistorical, WorkflowHistoricalOptions)
admin.site.register(WorkflowTransitionActionClass)
admin.site.register(WorkflowTransition, WorkflowTransitionAdmin)
