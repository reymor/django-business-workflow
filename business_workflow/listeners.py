# coding=utf-8
def set_state(sender, instance, **kwargs):
    if instance.pk is None:
        instance.current_state = instance.mw_workflow.get_initial_state()